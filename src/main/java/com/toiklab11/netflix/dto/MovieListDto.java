package com.toiklab11.netflix.dto;

import com.toiklab11.netflix.model.Movie;

import java.util.ArrayList;

public class MovieListDto {

    private ArrayList<Movie> movies;

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }



    public MovieListDto(ArrayList<Movie> movies){
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "MovieListDto{" +
                "movies=" + movies +
                '}';
    }
}
