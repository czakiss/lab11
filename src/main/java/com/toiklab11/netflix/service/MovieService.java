package com.toiklab11.netflix.service;

import com.toiklab11.netflix.dto.MovieListDto;

public interface MovieService {
    MovieListDto getMovies();
}
