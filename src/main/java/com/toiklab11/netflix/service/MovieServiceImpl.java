package com.toiklab11.netflix.service;

import com.toiklab11.netflix.dto.MovieListDto;
import com.toiklab11.netflix.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public MovieListDto getMovies() {
        return movieRepository.getMovies();
    }
}
