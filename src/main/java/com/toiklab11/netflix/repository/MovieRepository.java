package com.toiklab11.netflix.repository;

import com.toiklab11.netflix.dto.MovieListDto;

public interface MovieRepository {
    MovieListDto getMovies();
}
