package com.toiklab11.netflix.repository;

import com.toiklab11.netflix.dto.MovieListDto;
import com.toiklab11.netflix.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;

@Repository
public class MovieRepositoryImpl implements MovieRepository {



    private final ArrayList<Movie> movies = new ArrayList<>(Arrays.asList(
            new Movie(
                    1,
                    "Piraci z krzemowej doliny",
                    1999,
                   " https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg"
            ),
            new Movie(
                    2,
                    "Ja, robot",
                    2004,
                    "https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg"
            ),
            new Movie(
                    3,
                    "Kod nieśmiertelności",
                    2011,
                    "https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg"
            ),
            new Movie(
                    4,
                    "Ex Machina",
                    2015,
                    "https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg"
            )
    ));
    private final MovieListDto movieListDto = new MovieListDto(movies);

    @Override
    public MovieListDto getMovies() {
        return this.movieListDto;
    }
}
