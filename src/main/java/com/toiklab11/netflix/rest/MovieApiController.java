package com.toiklab11.netflix.rest;

import com.toiklab11.netflix.dto.MovieListDto;
import com.toiklab11.netflix.service.MovieService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MovieService movieService;

    @CrossOrigin
    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        MovieListDto movies = movieService.getMovies();
        LOGGER.info("[Controller] --- getting MovieList: {} ", movies);

        return new ResponseEntity<>(movies, HttpStatus.OK);
    }
}
